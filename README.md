# Upgrade OCP Java 6, 7 & 8 to Java SE 11 Developer
- https://education.oracle.com/en/java-se-11-programmer-i/pexam_1Z0-817
- Exam Number: `1Z0-817`
- preparation material from various sources

## Exploring Java 9: The Key Parts 
- by Venkat Subramaniam
- https://www.youtube.com/watch?v=Yacu1yUktjY
 
##  OCP Java SE 8 Programmer II Exam Guide
- by Kathy Sierra, Bert Bates, Elisabeth Robson

## OCP Oracle Certified Professional Java SE 11 Programmer I Study Guide: Exam 1Z0-815
- by Jeanne Boyarsky, Scott Selikoff 

## Exam 1Z0-817: Upgrade OCP Java 6, 7 & 8 to Java SE 11 Developer Study Guide
- Mikalai Zaikin
- http://java.boot.by/ocpjd11-upgrade-guide/index.html

## Whizlabs
- https://www.whizlabs.com/ocpjd-java-se-11-developer-upgrade-1z0-817/

