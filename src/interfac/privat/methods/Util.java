package interfac.privat.methods;

public interface Util {

    default int getNumberOfCore() {
        return helper();
    }

    private int helper() {
        return 4;
    }

    static int foo() {
        return helper2();
    }

    private static int helper2() {
        return 5;
    }
}
