package interfac;

/** interface with static members */
public interface StaticInterface {

  /**
   * public static final
   */
  String myField = "field in interface has to be initialized or compiler error";

  static String getName() {
    return "static method in interface";
  }
}
