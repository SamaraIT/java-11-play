package interfac;

import interfac.privat.methods.Util;

// 1st extends then implements, otherwise compiler error!
public class Bar extends AbstractBar implements StaticInterface, Util {

  static String myField = "static field in class with same name as in interface";

  // Subtypes of interface don't inherit static method, hence they can define a method with the exact same signature:
  public String getName() {
    return "method with the same name like in interface - but not static";
  }

  static String print() {
    return "static method in class";
  }

  // default method defined in Util interface
  /* static */ public int getNumberOfCore() { return 5; } // static method cannot override or hide a non-static one

  public static void main(String... a) {
    System.out.println("\n** Reference is interface");
    StaticInterface interfaceRef = new Bar();
//    interfaceRef.getName(); // Static method in interface may be invoked on containing interface class only - compiler error
    System.out.println("StaticInterface.getName(): " + StaticInterface.getName()); // calling on interface itself works


    System.out.println("interfaceRef.myField: " + interfaceRef.myField); // static field can be invoked on object/reference
    System.out.println("StaticInterface.myField: " + StaticInterface.myField); // or directly on interface class
    assert interfaceRef.myField.equals(StaticInterface.myField);
    assert interfaceRef.myField == StaticInterface.myField;
//    interfaceRef.myField = "compiler error!"; // Cannot assign a value to final variable 'myField'

    System.out.println("\n*** Reference is class");
    Bar bar = new Bar();
    System.out.println("bar.getName(): " + bar.getName());
    System.out.println("bar.myField: " + bar.myField);
    System.out.println("Bar.myField: " + Bar.myField);
    System.out.println("myField: " + myField);
    assert interfaceRef.myField.equals(bar.myField) == false;

    // static method in class can be invoked on reference or on class
    System.out.println("Bar.print(): " + Bar.print());
    System.out.println("bar.print(): " + bar.print());
    System.out.println("print(): " + print());

  }
}
