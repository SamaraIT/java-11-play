package interfac;


public abstract class ClassInheritanceAndOverride extends Boo implements Foo {

  @Override
  public abstract void methodB(); // normal interface

  @Override
  public abstract void methodA(); // method that was not abstract now is!

  // public static void m() { } // A static method cannot hide or override a non-static one.

}

class Boo {
  void methodB() {
    System.out.println("concrete method in concrete class");
  }
}

interface Foo {
  void methodA();

  default void m() {
    System.out.println("Foo.m - default method");
  }
}

interface Foo2 extends Foo {
  default void m() {
   Foo.super.m(); // referring to an overridden method in a super-interface, we must use the super keyword, prefixing it with the interface's name.
                  // Such a prefix is necessary because it helps eliminate ambiguity if a type implements or extends multiple interfaces.
  }
}
