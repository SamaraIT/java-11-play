package future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class SampleFuture {

  public static void main(String[] args) throws Exception {
    CompletableFuture<Integer> future = CompletableFuture.supplyAsync(SampleFuture::factory);
    future.thenAccept(System.out::println);

    future.completeOnTimeout(-1, 2, TimeUnit.SECONDS);
    // or
    future.orTimeout(2, TimeUnit.SECONDS);

    // wait for it to finish
    Thread.sleep(6000);
  }


  public static int factory()  {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return 45;
  }



}
