package nio;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathPlay {

  public static void main(String[] args) {
    Path rel1 = Paths.get("samara");
    Path rel2 = Paths.get("home/java/temp/music.mp3");
    Path abs1 = Paths.get("c:", "home");
    Path abs3 = Paths.get("c:/home/java/temp/music.mp3");

    System.out.println("\n ** Retrieving Information about absolute Path");
    System.out.println("toString: " + abs3.toString());         // c:/home/java/temp/music.mp3
    System.out.println("getFileName: " + abs3.getFileName());   // music.mp3
    assert abs3.getFileName().equals(abs3.getName(3));
    System.out.println("getName(1): " + abs3.getName(1));       // java
    System.out.println("getNameCount: " + abs3.getNameCount()); // 4
    System.out.println("getParent: " + abs3.getParent());       // c:\home\java\temp
    assert abs3.equals(abs3.getParent().resolve(abs3.getFileName()));
    System.out.println("getRoot: " + abs3.getRoot());           // c:\
    System.out.println("subpath(0,2): " + abs3.subpath(0, 2));  // home\java - Returns a relative Path that is a subsequence of the name elements of this path.

    System.out.println("\ntoString: " + abs1.toString());       // c:\home
    System.out.println("getFileName: " + abs1.getFileName());   // home
    System.out.println("getName(0): " + abs1.getName(0));       // home
    System.out.println("getNameCount: " + abs1.getNameCount()); // 1
    System.out.println("getParent: " + abs1.getParent());       // c:\
    assert abs1.equals(abs1.getParent().resolve(abs1.getFileName()));
    System.out.println("getRoot: " + abs1.getRoot());           // c:\
    assert abs1.getParent().equals(abs1.getRoot());             // in this case
    System.out.println("subpath(0,1): " + abs1.subpath(0, 1));  // home

    System.out.println("\n** Retrieving Information about relative Path");
    System.out.println("toString: " + rel2.toString());         // home\java\temp\music.mp3
    System.out.println("getFileName: " + rel2.getFileName());   // music.mp3
    System.out.println("getName(1): " + rel2.getName(1));       // java
    System.out.println("getNameCount: " + rel2.getNameCount()); // 4
    System.out.println("getParent: " + rel2.getParent());       // home\java\temp
    assert rel2.equals(rel2.getParent().resolve(rel2.getFileName()));
    System.out.println("getRoot: " + rel2.getRoot());           // null
    System.out.println("subpath(0,2): " + rel2.subpath(0, 2));  // home\java

    System.out.println("\n ** PRINT PATHS");
    System.out.println("abs1: " + abs1);
    System.out.println("abs3: " + abs3);
    System.out.println("rel1: " + rel1);
    System.out.println("rel2: " + rel2);

    System.out.println("\n ** Resolve: ");
    // If the other parameter is an absolute path then this method trivially returns other
    System.out.println("abs1.resolve(abs3) = abs3: " + abs1.resolve(abs3));
    assert abs1.resolve(abs3).equals(abs3);
    System.out.println("rel2.resolve(abs1) = abs1: " + rel2.resolve(abs1));
    assert rel2.resolve(abs1).equals(abs1);
    System.out.println("abs1.resolve(rel1) = abs1+rel1: " + abs1.resolve(rel1));
    System.out.println("abs3.resolve(rel2) = abs3+rel2: " + abs3.resolve(rel2));
    System.out.println("rel2.resolve(rel1) = rel2+rel1: " + rel2.resolve(rel1));
    //rel2.resolve(null); // won't compile - Ambiguous method call: Both resolve(Path) in Path and resolve(String) in Path match

    System.out.println("\n ** Relativize: ");
    System.out.println("abs1.relativize(abs3): " + abs1.relativize(abs3));
    System.out.println("abs3.relativize(abs1): " + abs3.relativize(abs1));
    System.out.println("rel2.relativize(rel1): " + rel2.relativize(rel1));
    System.out.println("rel1.relativize(rel2): " + rel1.relativize(rel2));
    try {
      System.out.println(abs3.relativize(rel1)); // java.lang.IllegalArgumentException: 'other' is different type of Path
    } catch (IllegalArgumentException e) {
      System.out.println("Cannot relativize mixed type paths -> IllegalArgumentException:" + e.getMessage());
    }

    System.out.println("\n ** Resolve sibling: "); // Resolves the given path against this path's parent path.
    // If this path does not have a parent path, or other is absolute, then this method returns other.
    System.out.println("abs1.resolveSibling(abs3) = abs3: " + abs1.resolveSibling(abs3));
    System.out.println("rel2.resolveSibling(abs1) = abs1: " + rel2.resolveSibling(abs1));
    // (getParent() == null) ? other : getParent().resolve(other);
    System.out.println("abs1.resolveSibling(rel1) = abs1.parent + rel1: " + abs1.resolveSibling(rel1));
    assert abs1.resolveSibling(rel1).equals(Path.of("c:", "samara"));
    System.out.println("abs1.resolveSibling(rel2) = abs1.parent + rel2: " + abs1.resolveSibling(rel2));
    System.out.println("rel2.resolveSibling(rel1) = rel2.parent + rel1: " + rel2.resolveSibling(rel1));
    System.out.println("rel1.resolveSibling(rel2) = rel1.parent + rel2 = null + rel2: " + rel1.resolveSibling(rel2));
    assert rel1.resolveSibling(rel2).equals(rel2);

    Path path = Path.of("C:", "home", "foo", "test.txt");
    System.out.println("\nUsing Path.of method: " + path);
    URI uri = URI.create("file:///D:/edukacija/Java/Effective%20java%203.pdf");
    Path pathFromUri = Paths.get(uri);
    System.out.println("Creating path from URI - Paths.get(uri): " + pathFromUri);
    System.out.println("Files.exists: " + Files.exists(pathFromUri));

    System.out.println("\n ** Normalize: ");
    Path p1 = Path.of("/a/../../b/././c");
    assert p1.getNameCount() == 7;
    // /../.. does not effect root, it is the same as /
    System.out.println(p1.normalize()); // /b/c
    Path p2 = Path.of("/a/../../");
    System.out.println(p2.normalize()); // /
    Path p3 = Path.of("a/../../b/././c");
    System.out.println(p3.normalize()); // ../b/c

    System.out.println("\n ** Equals && toAbsolutePath");
    Path log1 = Paths.get("log", ".");
    System.out.println("log1 = " + log1);
    Path log2 = Path.of(log1.getParent().toString(), ".");
    System.out.println("log2 = " + log2);
    assert log1.equals(log2);
    Path log1Abs = log1.toAbsolutePath();
    System.out.println("log1Abs = " + log1Abs);
    File log1ToFile = log1.toFile();
    System.out.println("log1ToFile = " + log1ToFile);
    System.out.println("log1.equals(log2) =" + log1.equals(log2));
    assert log1.equals(log2) == true;
    System.out.println("log1.equals(log1Abs) =" + log1.equals(log1Abs));
    assert log1.equals(log1Abs) == false;
    System.out.println("log1.equals(log1ToFile) =" + log1.equals(log1ToFile));
    assert log1.equals(log1ToFile) == false;

    Path a = Path.of("a/b");
    Path b = Path.of("a/b");
    System.out.println("\nrelativize same paths=" + a.relativize(b));
    assert a.relativize(b).equals(Path.of(""));

    System.out.println("relativize, other = null -> NPE");
    Path other = null;
    abs1.relativize(other); // NPE
  }
}
