package nio;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * FileVisitor walks recursively through a directory tree.
 * .
 * <p>
 * You can override one or all of the methods of SimpleFileVisitor:
 * <li>preVisitDirectory
 * <li>visitFile
 * <li>visitFileFailed
 * <li>postVisitDirectory
 * <p>
 * You can change the flow of a file visitor by returning one of the FileVisitResult constants: CONTINUE, SKIP_SUBTREE, SKIP_SIBLINGS, or TERMINATE.
 * <p>
 * Note that Java goes down as deep as it can before returning back up the tree. This is called a depth-first search.
 * We said “might” because files and directories at the same level can get visited in either order
 */
public class FileVisitorPlay {

  public static void main(String[] args) throws IOException {
    MyPathMatcher myPathMatcher = new MyPathMatcher();
    // UNIX doesn’t see the backslash as a directory boundary.
    // Use / instead of \\ so your code behaves more predictably across operating systems.
    Files.walkFileTree(Paths.get("d:/tools"), myPathMatcher);

  }
}

class MyPathMatcher extends SimpleFileVisitor<Path> {

  // ?	exactly one character
  // *	any character except for a directory boundary
  // **	any character, including a directory boundary; zero or more characters, including multiple directories
  private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**.txt");

  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
    if (matcher.matches(file))
      System.out.println(file);
    return FileVisitResult.CONTINUE;
  }
}
