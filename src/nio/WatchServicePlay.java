package nio;

import java.io.IOException;
import java.nio.file.*;

public class WatchServicePlay {

  public static void main(String[] args) throws IOException {
    WatchService watcher = FileSystems.getDefault().newWatchService();
    Path dir = Paths.get("d:/edukacija");
    dir.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
  }
}
