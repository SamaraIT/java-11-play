package nio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;

public class FilesPlay {

  public static void main(String[] args) throws IOException {
    System.out.println("Deleting file which does not exists");
    File file = new File("something");
    boolean delete = file.delete();
    System.out.println("file.delete: " + delete);

    delete = Files.deleteIfExists(file.toPath());
    System.out.println("Files.deleteIfExists: " + delete);
//    Files.delete(file.toPath()); // throws NoSuchFileException (IOException)

    System.out.println("\n*** Creating and deleting directories");
    Path directories = Path.of("a/b/c.txt");
    System.out.println("dir: " + directories);
    Path createDirectories = Files.createDirectories(directories); // c.txt is directory - weird name
    System.out.println("Files.createDirectories: " + createDirectories); // sometimes it is absolute and sometimes relative path!
    System.out.println("Files.isRegularFile: " + Files.isRegularFile(directories));
    assert Files.isRegularFile(directories) == false;
    System.out.println("Files.isDirectory: " + Files.isDirectory(directories));
    assert Files.isDirectory(directories) == true;
    System.out.println("directories == createDirectories: " + (directories == createDirectories)); // sometimes true sometimes false - if createDirectories returns relative then it is true
    System.out.println("directories.equals(createDirectories): " + directories.equals(createDirectories)); // sometimes true sometimes false - if createDirectories returns relative than it is true

    Path directory = Files.createDirectory(Path.of("samara"));
    System.out.println("Files.createDirectory: " + directory);
    System.out.println("Files.exists: " + Files.exists(directory)); // true
    System.out.println("Files.deleteIfExists(directory): " + Files.deleteIfExists(directory)); //true

    System.out.println("Files.deleteIfExists(directories): " + Files.deleteIfExists(directories));  // only 'c.txt' directory is deleted
    Files.deleteIfExists(directories.subpath(0,2)); // delete b directory
    Files.deleteIfExists(directories.subpath(0,1)); // delete a directory

    System.out.println("Files.exists(directories): " + Files.exists(directories));
    assert Files.exists(directories) == false;

    System.out.println("\n*** Copy and move");
    Path one = Path.of("one.txt");
    if (Files.notExists(one))
      Files.createFile(one);
    System.out.println("Files.isRegularFile: " + Files.isRegularFile(one));
    assert Files.isRegularFile(one) == true;
    Path targ = Path.of("two.txt");
    System.out.println("Files.exists - before copy targ: " + Files.exists(targ));
    assert Files.exists(targ) == false;
    Files.copy(one, targ);
//    Files.copy(one, targ, StandardCopyOption.REPLACE_EXISTING);
    System.out.println("Files.exists - after copy targ : " + Files.exists(targ));
    assert Files.exists(targ) == true;
    Files.move(one, targ, StandardCopyOption.REPLACE_EXISTING); //  this method attempts to move the file to the target file,
    // failing if the target file exists except if the source and target are the same file, in which case this method has no effect.
    System.out.println("Files.exists - after move one: " + Files.exists(one));
    assert Files.exists(one) == false;
    Files.move(targ, targ); // no effect since source and target are the same
    Files.deleteIfExists(targ);
    assert Files.exists(targ) == false;

    System.out.println("\n* Files.writeString");
    Path p = Path.of("text.txt");
//    Files.createFile(p); // no need if StandardOpenOption.CREATE is used in writeString method
    Files.writeString(p, "Hello", StandardOpenOption.CREATE, StandardOpenOption.APPEND, StandardOpenOption.WRITE);
    Files.writeString(p, "Goodbye", StandardOpenOption.CREATE, StandardOpenOption.APPEND, StandardOpenOption.WRITE);
    String read = Files.readString(p);
    assert "HelloGoodbye".equals(read);
    System.out.println("Files.readString after writing with StandardOpenOption.APPEND: " + read);
    //  Files.writeString has TRUNCATE_EXISTING options as default:
    Files.writeString(p, "Haha"); // DEFAULT_OPEN_OPTIONS = Set.of(StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
    read = Files.readString(p);
    assert "Haha".equals(read);
    System.out.println("Files.readString after writing with default options: " + read);
    boolean b = Files.deleteIfExists(p);
    System.out.println("Files.deleteIfExists: " + p + " returns " + b);
    assert Files.exists(p) == false;

    try {
      System.out.println("\n* Using Files.writeString with wrong combination of StandardOpenOption throws IllegalArgumentException:");
      Files.writeString(p, "Hello ", StandardOpenOption.APPEND, StandardOpenOption.TRUNCATE_EXISTING);
    } catch (IllegalArgumentException e) {
      System.err.println(e.getMessage());
    }
  }
}

