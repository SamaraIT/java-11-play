package nio.attribute;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.*;
import java.time.*;

public class AttributesPlay {

  public static void main(String[] args) throws IOException {
    ZonedDateTime janFirstDateTime = ZonedDateTime.of(
      LocalDate.of(2017, 1, 1),
      LocalTime.of(10, 0),
      ZoneId.of("Europe/Zagreb"));
    Instant januaryFirst = janFirstDateTime.toInstant();

    Path dir = Path.of("c:/tmp");
    Files.createDirectories(dir); // it will not throw exception if directory already exists
    Path path = dir.resolve("file1");
    if (Files.notExists(path))
      Files.createFile(path);
    System.out.println("Created file for demo: " + path);
    FileTime fileTime = FileTime.from(januaryFirst);
    Files.setLastModifiedTime(path, fileTime);

    System.out.println("\n**  read attributes using Files static methods:");
    System.out.println("modifiedTime: " + Files.getLastModifiedTime(path));
    assert fileTime.equals(Files.getLastModifiedTime(path));
    System.out.println("isExecutable: " + Files.isExecutable(path));
    assert Files.isExecutable(path) == true;
    System.out.println("isReadable: " + Files.isReadable(path));
    assert Files.isReadable(path) == true;
    System.out.println("isWritable: " + Files.isWritable(path));
    assert Files.isWritable(path) == true;
    System.out.println("getOwner: " + Files.getOwner(path));

    System.out.println("\n**  read attributes using BasicFileAttributes");
    BasicFileAttributes basic = Files.readAttributes(path, BasicFileAttributes.class);
    System.out.println("creationTime: " + basic.creationTime());
    System.out.println("lastAccessTime: " + basic.lastAccessTime());
    System.out.println("lastModifiedTime: " + basic.lastModifiedTime());
    assert basic.lastModifiedTime().equals(Files.getLastModifiedTime(path));
    System.out.println("isDirectory: " + basic.isDirectory());
    assert basic.isDirectory() == false;
    System.out.println("isRegularFile: " + basic.isRegularFile());
    assert basic.isRegularFile() == true;

    System.out.println("\n**  view is for updating - BasicFileAttributeView.setTimes");
    BasicFileAttributeView basicView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
    FileTime accessTime = FileTime.fromMillis(System.currentTimeMillis());
    basicView.setTimes(null, accessTime, null);
    System.out.println("lastAccessTime set to now (System.currentTimeMillis())");

    System.out.println("\nFiles.setAttribute: dos:hidden & dos:readonly");
    Files.setAttribute(path, "dos:hidden", true);
    Files.setAttribute(path, "dos:readonly", true);

    System.out.println("\n**  read attributes using Files.getAttribute");
    System.out.println("dos:hidden: " + Files.getAttribute(path, "dos:hidden"));
    System.out.println("dos:readonly:" + Files.getAttribute(path, "dos:readonly"));

    System.out.println("\n**  read attributes using DosFileAttributes");
    DosFileAttributes dos = Files.readAttributes(path, DosFileAttributes.class);
    System.out.println("dos.lastAccessTime: " + dos.lastAccessTime());
    assert dos.lastAccessTime().equals(accessTime);
    assert !dos.lastAccessTime().equals(basic.lastAccessTime()) : "lastAccessTime wasn't updated?";
    System.out.println("dos.isHidden: " + dos.isHidden());
    assert dos.isHidden() == true;
    System.out.println("dos.isReadOnly:" + dos.isReadOnly());
    assert dos.isReadOnly() == true;
    System.out.println("dos.isSystem:" + dos.isSystem());
    assert dos.isSystem() == false;

//    Set<PosixFilePermission> perms = PosixFilePermissions.fromString("r--r--"); // java.lang.IllegalArgumentException: Invalid mode
//    Files.setPosixFilePermissions(path, perms);
//    PosixFileAttributes posix = Files.readAttributes(path, PosixFileAttributes.class);
//    System.out.println("posix.group(): " + posix.group());
//    System.out.println("posix.permissions():" + posix.permissions());

    try {
      System.out.println("\n** cannot delete readonly file - AccessDeniedException");
      Files.delete(path);
    } catch (IOException e) {
      e.printStackTrace(); // java.nio.file.AccessDeniedException: c:\tmp\file1
    }

    System.out.println("\n**  update readonly attribute via DosFileAttributeView");
    DosFileAttributeView dosView = Files.getFileAttributeView(path, DosFileAttributeView.class);
    dosView.setReadOnly(false);

    System.out.println("* Delete file using Files.delete");
    Files.delete(path);
  }
}
