package nio.streams;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

public class GlobPlay {

  public static void main(String[] args) throws IOException {
    // true:
    Path path1 = Paths.get("0$b/test1");
    Path path2 = Paths.get("0$ABC/sss1");
    // false:
    Path path3 = Paths.get("10$aaa/sss1"); // starts with 2 numbers
    Path path4 = Paths.get("0e$bB/sss1"); // e between number and $
    Path path5 = Paths.get("0$C/test1"); // C after $
    Path path6 = Paths.get("0$b2"); // ends with 2 instead 1
    Path path7 = Paths.get("0$b/sss2"); // // ends with 2 instead 1
    // true:
    Path path8 = Paths.get("0$Bsomeletters1"); // B instead b
    Path path9 = Paths.get("0$AAAsomeletters1"); // many AAA
    Path path10 = Paths.get("0$asomeletters1"); // a instead A

    final String glob = "glob:[0-9]${A*,b}**1";
    // starts with a number followed by $ than word that starts with A/a or b/B than any char including directories and ends with 1

    PathMatcher matcher = FileSystems.getDefault().getPathMatcher(glob);
    System.out.println("1: " + matcher.matches(path1)); // true
    System.out.println("2: " + matcher.matches(path2)); // true
    System.out.println("3: " + matcher.matches(path3)); // false - starts with 2 numbers
    System.out.println("4: " + matcher.matches(path4)); // false - e between 0 and $
    System.out.println("5: " + matcher.matches(path5)); // false - C after $
    System.out.println("6: " + matcher.matches(path6)); // false - ends with 2 instead 1
    System.out.println("7: " + matcher.matches(path7)); // false - ends with 2 instead 1
    System.out.println("8: " + matcher.matches(path8)); // true
    System.out.println("9: " + matcher.matches(path9)); // true
    System.out.println("10: " + matcher.matches(path10)); // true
  }

}
