package nio.streams;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * DirectoryStream only looks at files in the immediate directory.
 */
public class FileStreams {

  public static void main(String[] args) throws IOException {
    Path dir = Paths.get("c:/Users");

// glob expression [az]* finds files that starts with a or z
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "[az]*")) {
      for (Path path : stream)
        System.out.println(path.getFileName());
    }

    // ?	exactly one character
    // *	any character except for a directory boundary
    // **	any character, including a directory boundary; zero or more characters, including multiple directories

  }
}
