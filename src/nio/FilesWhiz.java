package nio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Files.list, walk and find methods
 */
public class FilesWhiz {

  public static void main(String[] args) throws IOException {
    Path root = Path.of("root");
    Path file1 = Path.of("root/dir/f.txt");
    Path file2 = Path.of("root/dir/z.txt");
    Files.createDirectories(file1.getParent());
    if (Files.notExists(file1))
      Files.createFile(file1);
    if (Files.notExists(file2))
      Files.createFile(file2);

    System.out.println("\n** Files.list");
    Files.list(root) // returns stream describing the content of the directory
      // The elements of the stream are Path objects that are obtained as if by resolving the name of the directory entry against dir.
      .forEach(System.out::println); // root\dir - there is only file, directory inside root folder

    System.out.println("\n** Files.walk");
    // The file tree is traversed depth-first, the elements in the stream are Path objects that are obtained as if by resolving the relative path against start.
    Files.walk(root) // path has to exists - NoSuchFileException
      .forEach(System.out::println); // root    root\dir    root\dir\f.txt    root\dir\z.txt

    long count = Files.walk(root, 2)
      .count();
    System.out.println("\nFiles.walk.count= " + count); // 4
    assert count == 4;

    System.out.println("\n** Files.find"); // similar to walk method with filter
    Files.find(root, 2,
      (path, attributes) -> true) // no filter - BiPredicate
      .forEach(System.out::println); // root    root\dir    root\dir\f.txt    root\dir\z.txt

    count = Files.find(root, 2,
      (path, attributes) -> true) // no filter - since it always returns true
      .count();
    System.out.println("Files.find.count= " + count); // 4
    assert count == 4;

    String txtFiles = Files.find(root, 2,
      (path, attributes) -> String.valueOf(path).endsWith(".txt")) // filter all files that end with .txt
      .map(String::valueOf)       // if the argument is null, then a string equal to "null"; otherwise, the value of obj.toString() is returned.
      .collect(Collectors.joining("; "));
    System.out.println("\nFound txt files in root directory with depth 2: " + txtFiles); // root\dir\f.txt; root\dir\z.txt

    System.out.println("\n*** cleanup - Files.walk -> File::delete");
    Files.walk(root)     // start from the 1st dir ('a') from relative directory 'a/b/c.txt'
      .sorted(Comparator.reverseOrder())
      .map(Path::toFile)                   // using File so we do not have to catch exception
      .peek(System.out::println)
      .forEach(File::delete);
/*        try {
          Files.deleteIfExists(f);
        } catch (IOException e) {
          e.printStackTrace();
        }
    });*/

  }
}
