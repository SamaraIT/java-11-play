package nio;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Classes that do not implement <em>Serializable</em> interface will not have any of their state serialized or deserialized. <br>
 * All subtypes of a serializable class are themselves serializable. <br>
 * During deserialization, the fields of non-serializable classes will be initialized using the public or protected no-arg constructor of the class.
 * A no-arg constructor must be accessible to the subclass that is serializable. The fields of serializable subclasses will be restored from the stream.
 */
public class SerializationPlay {

  public static void main(String[] args) throws IOException, ClassNotFoundException {
    String file = "test.ser";
    C input = new C(108);
    System.out.println("\nbefore serialization: " + input);
    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
         ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));) {
      System.out.println("\noos.writeObject");
      oos.writeObject(input);
      System.out.println("ois.readObject:");
      Object object = ois.readObject(); // only constructor of A is called (not serializable class - default constructor is called)
      System.out.println("\nafter de-serialization: " + object);
    }
    Files.deleteIfExists(Path.of(file));
  }
}

class A {
  int a;

  public A() {
    System.out.println("A " + a);
  }  // mandatory for deserialization to happen
     // else java.io.InvalidClassException: nio.C; no valid constructor

  public A(int a) {
    this.a = a;
    System.out.println("A " + a);
  }
}

class B extends A implements Serializable {
  int b;

  public B(int b) {
    super(b);
    this.b = b;
    System.out.println("B " + b);
  }
}

class C extends B {
  int c;

  public C(int c) {
    super(c);
    this.c = c;
    System.out.println("C " + c);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("C{");
    sb.append("a=").append(a);
    sb.append(", b=").append(b);
    sb.append(", c=").append(c);
    sb.append('}');
    return sb.toString();
  }
}
