package se11.functional;

import java.util.Random;
import java.util.function.*;

public class IntSupplierTest {

  public static void main(String[] args) {
    Random random = new Random();
    Consumer c; Predicate p; Supplier s;
    Function f; UnaryOperator op; BiFunction bf;

    IntSupplier supplier = () -> random.nextInt();
    int myRandom = supplier.getAsInt();
    System.out.println("Random: " + myRandom);
  }
}
