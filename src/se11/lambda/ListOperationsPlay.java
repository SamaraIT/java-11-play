package se11.lambda;

import java.util.Arrays;
import java.util.List;

public class ListOperationsPlay {
  public static void main(String[] args) {
//    List<Integer> numbers_2 = List.of(1, 2, 3); //UnsupportedOperationException
    List<Integer> numbers_2 = Arrays.asList(1, 2, 3);
    numbers_2.replaceAll(t -> t + 1);
    numbers_2.forEach(System.out::println);
  }
}
