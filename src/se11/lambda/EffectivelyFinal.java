package se11.lambda;

import java.util.ArrayList;
import java.util.List;

public class EffectivelyFinal {

  public static void main(String[] args) {
    remove(new ArrayList<>());
  }

  public static void remove(List<Character> chars) {
    char end = 'z';
// INSERT LINE HERE
//  chars = null; // Exception in thread "main" java.lang.NullPointerException

    chars.removeIf(c -> {
      char start = 'a';
      return start <= c && c <= end;
    });

    // INSERT LINE HERE
    char start = 'a';
    char c = 'x';
    chars = null;
//  end = '1'; // compiler error - end cannot be used in lambda - it is no longer final
  }
}
