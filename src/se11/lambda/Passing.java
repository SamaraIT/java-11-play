package se11.lambda;

import java.util.Comparator;
import java.util.function.Consumer;

public class Passing {
  public void method() {
    x((String x) -> {}, (Boolean x, Boolean y) -> 0);
    x((var x) -> {}, (var x, var y) -> 0);
    var samara = 1;
  }
  public void x(Consumer<String> x, Comparator<Boolean> y) {
  }
}
