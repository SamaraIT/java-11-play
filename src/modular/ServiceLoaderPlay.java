package modular;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

public class ServiceLoaderPlay {

  public static void main(String[] args) {
    System.out.println("get providers using ServiceLoader");
    // Returns a stream to lazily load available providers of this loader's service. 
    // The stream elements are of type Provider, the Provider's get method must be invoked to get or instantiate the provider.
    List<Set> setProviders = ServiceLoader.load(Set.class)
      .stream()
      .map(ServiceLoader.Provider::get)
      .collect(Collectors.toList());
    System.out.println(setProviders);

    List<List> listProviders = new ArrayList<>();
    ServiceLoader.load(List.class).forEach(listProviders::add);
    System.out.println(listProviders);

  }
}
