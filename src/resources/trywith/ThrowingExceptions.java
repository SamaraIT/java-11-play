package resources.trywith;

import java.io.IOException;

/**
 * https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html#suppressed-exceptions
 */
public class ThrowingExceptions {

  public static void main(String[] args) {
    System.out.println("START");
    try (MyResource myResource = new MyResource()) {
      myResource.open();
      throw new NullPointerException("try"); // unreachable - open will throw exception
    } catch (IOException e) {
      System.out.println("* in catch block:");
      System.out.println("exc: " + e.getClass() + ", msg: " + e.getMessage());
      Throwable suppressed = e.getSuppressed()[0];
      System.out.println("suppressed: " + suppressed.getClass() + ", msg: " + suppressed.getMessage());
    } finally {
      System.out.println("* in finally block:");
      throw new ClassCastException("finally block is executed last");
    }
  }
}
