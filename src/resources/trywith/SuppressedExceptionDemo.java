package resources.trywith;

import java.io.IOException;

/**
 * https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html#suppressed-exceptions
 */
public class SuppressedExceptionDemo {

  public static void main(String[] args) throws IOException {
    System.out.println("Example of suppressed exception in try-with-resources");
    try (MyResource myResource = new MyResource()) {
      myResource.open(); // Exception in thread "main" java.io.IOException: open
                         //     Suppressed: java.lang.ArithmeticException: close
      throw new NullPointerException("try");
    }
//    } catch (IOException e) {
//      e.printStackTrace();
//      System.err.println(Arrays.toString(e.getSuppressed()));
//    }
  }
}

class MyResource implements AutoCloseable {

  @Override
  public void close() throws ArithmeticException {
    System.out.println("close throws ArithmeticException");
    throw new ArithmeticException("close");
  }

  public void open() throws IOException {
    System.out.println("myResource is doing operation");
    throw new IOException("open");
  }
}
