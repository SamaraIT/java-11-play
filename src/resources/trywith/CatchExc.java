package resources.trywith;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CatchExc {

  public static void main(String[] args) {

    try (var reader = new BufferedReader(new FileReader("samara.txt"))) {
      String line = null;
      while ((line = reader.readLine()) != null) {
        System.out.println(line);
      }
    } catch (FileNotFoundException e) { // FileNotFoundException extends IOException - has to be declared before superclass
    } catch (IOException e) {
//    } catch (FileNotFoundException | IOException e) { // DOES NOT COMPILE - classes are from the same inheritance tree
    }

  }

}
