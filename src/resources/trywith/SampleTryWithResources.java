package resources.trywith;

class Resource implements AutoCloseable {

  private final String name;

  public Resource(String name) {
    this.name = name;
    System.out.println("created... within constructor " + name);
  }

  @Override
  public void close() throws Exception {
    System.out.println("close - clean up " + name);
  }

  public void op1() {
    System.out.println("op1 " + name);
  }
}

/**
 * Up until Java 8, the try-with-resources statement could only close resources declared inside the statement itself.
 * As of Java 9, this statement can also close predefined resources, provided those resources are referenced within parentheses immediately after the <em>try</em> keyword.
 */
public class SampleTryWithResources {

  public static void main(String[] args) throws Exception {
    System.out.println("START");

    Resource resource = new Resource("1"); // resource was passed into the function - has to be effectively final
    try (Resource r2 = new Resource("2"); resource; resource) { // should you close resource or not?
      resource.op1();
    } // close: 1, 1, 2
    System.out.println("DONE: after try-with-resources block");

    // allowed variable name:
    int __ = 4;

    try (resource) { // should you close resource or not?
      resource.op1();
    } // close 1
  }
}
