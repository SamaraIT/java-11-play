package var;

import java.util.function.UnaryOperator;

public class VarPlay {
  // var i = 0;        // not permitted as instance variable - compiler error
  // static var i = 0; // not permitted as class field - compiler error

  static {
    var samara = "Can be used in static initialized blocks";
    System.out.println(samara);
    new VarPlay();
  }

  {
    var samara = "Can be used in initialized blocks";
    System.out.println(samara);
  }

  public static void main(String[] args) {
    System.out.println("\n ** var play");
    var v4 = 1L + 1F; // inferred to float
    System.out.println("long + float = float: " + v4);

    System.out.println("\nvar has to be initialized: ");
    var result = 0; // int
    System.out.println(result);

    System.out.println("combining int and float => float: ");
    int i1 = 1;
    int i2 = 2;
    float i3 = i1++ + ++i2; // int -> float
    System.out.println(i3); // 4.0
    var r = (i1 + i2) / i3; // (2+3)/4 = 1.25 (float)
    System.out.println(r);
//    result = r; // compiler error= incompatible types: possible lossy conversion from float to int

    System.out.println("\nCan be assigned null value with explicit cast");
    var x = (Integer) null; // bad usage

    System.out.println("\n - Not allowed in compound declaration");
    // var x, y = 0;

    System.out.println(" - Not allowed with array initializer");
    // var arr = {1, 2, 3};

    System.out.println(" - Not allowed with method reference");
    // var unaryOp = String::toLowerCase;

    System.out.println("\nFrom Java 11 you can use the var reserved type name as lambda expression parameter type - enables you to add annotations or final modifier to lambda parameters");
    UnaryOperator<String> lc = (@Notnull final var s) -> s.toLowerCase();

    System.out.println("\nIllegal lambda expression type inference usage:");
    System.out.println(" - Not allowed to use var for some parameter and skip for other");
    //BiFunction<String, String, String> bf = (var a, b) -> a + b;

    System.out.println(" - Not allowed to mix var with explicit type");
    // BiFunction<String, String, String> bf2 = (var a, String b) -> a + b;

    System.out.println(" - Not allowed to skip parentheses in single parameter lambda while using var");
    // UnaryOperator<String> unaryOp = var s -> s.toLowerCase();
  }

}

@interface Notnull {}
