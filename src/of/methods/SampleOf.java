package of.methods;

import java.util.*;

public class SampleOf {

  public static void main(String[] args) {
    System.out.println("** Java 9: List.of, Set.of, Map.of");

    Map<String, ? extends Number> map = Map.of("a", 1, "b", 2, "c", 3.0);
    System.out.println("map: " + map);
    System.out.println("map.getClass(): " + map.getClass()); // class java.util.ImmutableCollections$MapN

    List<Integer> numbers_2 = List.of(1, 2, 3);
    System.out.println("\nList.of -> numbers_2.getClass(): " + numbers_2.getClass()); // class java.util.ImmutableCollections$ListN
    System.out.println("* Cannot sort or add in ImmutableCollections");
    try {
      numbers_2.sort(Comparator.comparing(Integer::intValue)); // can not be sorted
      System.out.println(numbers_2);
      numbers_2.add(7);
    } catch (UnsupportedOperationException e) {
      System.out.println("UnsupportedOperationException occurs " + e.getMessage());
    }

    System.out.println("\n* Java 8 way: Arrays.asList returns fixed-size list backed by the specified array");
    List<Integer> numbers_1 = Arrays.asList(1, 32, 3); //  fixed-size list backed by the specified array
    System.out.println("numbers_1.getClass(): " + numbers_1.getClass()); // java.util.Arrays$ArrayList
    System.out.println("* Cannot add in List that is backed by array, but can sort");
    try {
      numbers_1.sort(Comparator.comparing(Integer::intValue)); // can be sorted
      System.out.println(numbers_1);
      numbers_1.add(4);
    } catch (UnsupportedOperationException e) {
      System.out.println("UnsupportedOperationException occurs " + e.getMessage());
    }

    try {
      Set.of("A", "B", null, "D");
    } catch (NullPointerException e) {
      System.out.println("\n* Null values are disallowed when creating collections using 'of' static method " + e.getMessage());
    }

    System.out.println("\n* Duplicates in Sets and Maps cause an IllegalArgumentException");
    try {
      Set.of("A", "B", "A");
    } catch (IllegalArgumentException e) {
      System.out.println("e.getMessage(): " + e.getMessage());
    }

    System.out.println("\n* Duplicates in Lists are allowed");
    List<String> stringList = List.of("A", "B", "A");
    System.out.println("stringList: " + stringList);
    System.out.println("Creating set from list with duplicates");
    HashSet hashSet = new HashSet(stringList);
    System.out.println("hashSet: " + hashSet);
    boolean add = hashSet.add("C");
    assert true == add;
    System.out.println("hashSet - added C: " + hashSet);
    add = hashSet.add("A");
    assert false == add;
    System.out.println("hashSet after adding A - set is unchanged: " + hashSet);
  }
}
