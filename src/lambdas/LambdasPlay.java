package lambdas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class LambdasPlay {

  public static void main(String[] args) {
    List<String> bunnies = new ArrayList<>();
    bunnies.add("one");
    bunnies.add("hi");
    bunnies.add("two");
    bunnies.add("alo");
    bunnies.add("Alo");
    bunnies.forEach(System.out::println);

    System.out.println("\n *** list.sort");
    bunnies.sort(String::compareTo);
    bunnies.forEach(System.out::println);

    System.out.println("\n *** list.replaceAll - UnaryOperator");
    bunnies.replaceAll(t -> t + "_" + t.length());
    bunnies.forEach(System.out::println);

    System.out.println("\n *** list.removeIf it does not start with h");
    bunnies.removeIf(s -> s.charAt(0) != 'h');
    bunnies.forEach(System.out::println);
    System.out.println("isEmpty: " + bunnies.isEmpty());
    assert bunnies.size() == 1;

    System.out.println("\n *** map.forEach on keySet, values, and whole map using BiConsumer");
    Map<String, Integer> bunniesMap = new HashMap<>();
    bunniesMap.put("long ear", 3);
    bunniesMap.put("floppy", 8);
    bunniesMap.put("hoppy", 1);
    System.out.println("map.keySet().forEach");
    bunniesMap.keySet().forEach(b -> System.out.println(b));
    System.out.println("map.values().forEach");
    bunniesMap.values().forEach(System.out::println);
    System.out.println("map.forEach");
    bunniesMap.forEach((k, v) -> System.out.println(k + " " + v));

    System.out.println("\n *** map.compute calls");
    bunniesMap.computeIfAbsent("computeIfAbsent", k -> k.length()); // If the specified key is not already associated with a value (or is mapped to null), attempts to compute its value using the given mapping function and enters it into this map unless null.
    System.out.println("after computeIfAbsent: " + bunniesMap.get("computeIfAbsent"));
    bunniesMap.computeIfPresent("computeIfAbsent", (k, old) -> old + k.length());
    System.out.println("after computeIfPresent: " + bunniesMap.get("computeIfAbsent"));
    bunniesMap.compute("samara", (k, old) -> 100008);
    bunniesMap.forEach((k, v) -> System.out.println(k + " " + v));

    System.out.println("\n *** map.replaceAll - increase values by 2 - BiFunction");
    bunniesMap.replaceAll((k, v) -> v * 2);
    bunniesMap.forEach((k, v) -> System.out.println(k + " " + v));

    System.out.println("UnaryOperator vs Function signature");
    System.out.format("Total = %.2f", computeTax(10.00, (p) -> p * 0.05));
    System.out.format("Total = %.2f", computeTaxFuction(10.00, (p) -> p * 0.05));
  }

  static double computeTax(double price, UnaryOperator<Double> op) {
    return op.apply(price) + price;
  }

  static double computeTaxFuction(double price, Function<Double, Double> op) {
    return op.apply(price) + price;
  }
}
