package lambdas;

import java.util.function.Function;
import java.util.function.Supplier;

public class InstantiateClass {

  public static void main(String[] args) {
    Supplier<Person> supplier = Person::new;
    Person person1 = supplier.get();
    System.out.println(person1.getName());

    Function<String, Person> func = Person::new;
    Person person2 = func.apply("Function");
    System.out.println(person2.getName());
  }
}

class Person {
  String name;

  public Person() {
    this("No name");
  }

  public Person(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
