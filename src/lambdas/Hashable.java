package lambdas;

@FunctionalInterface
public interface Hashable {
  int hash(Object o, Integer i, String... a);

  // When lambda expression targets a functional interface, all the parameters must be exactly the same!
  public static void main(String[] args) {
    Hashable h1 = (Object o, Integer i, String[] a) -> o.hashCode() + i + a.length;
//    Hashable h2 = (Object o, int i, String[] a) -> o.hashCode() + i + a.length; // incompatible types
//    Hashable h3 = (String o, Integer i, String[] a) -> o.hashCode() + i + a.length; // incompatible types
  }
}
