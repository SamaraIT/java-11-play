package lambdas;

/**
 * https://docs.oracle.com/javase/specs/jls/se11/html/jls-15.html#jls-15.27
 * <p>
 * Variable used in lambda expression should be final or effectively final
 */
public class LambdaInForLoops {

  static void foo(Foo function) {
    System.out.print(function.m());
  }

  public static void main(String[] args) {
    args = new String[]{"a", "b"};
    for (String i : args) {
      foo(() -> i);
    } // ab

    for (int i = 0; i < args.length; i++) {
//      foo(() -> args[i]); // i is not final! -> compiler error
    }
  }
}

interface Foo {
  String m();
}
