package lambdas;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * When lambda expression targets a functional interface, all the parameters must be exactly the same!
 */
public class WhizlabsQuestions {

  public static void main(String[] args) {
    System.out.println("* Cannot infer functional interface type");
    Integer[] integers = {3, 2, 1};
    System.out.println(Arrays.toString(integers));

    Arrays.sort(integers, Integer::compareTo);      // public int compareTo(Integer anotherInteger)
    Arrays.sort(integers, Integer::compare);        // public static int compare(int x, int y)
    Arrays.sort(integers, Comparator.comparingInt((var i) -> i));
    Arrays.sort(integers, Comparator.comparingInt((Integer i) -> i));
//    Arrays.sort(integers, Comparator.comparingInt((int i) -> i)); // Cannot infer functional interface type
    Arrays.sort(integers, (var i1, var i2) -> i1 - i2);
    Arrays.sort(integers, (Integer i1, Integer i2) -> i1 - i2);     // has to be Integer, when using lambda - exact match is required
//    Arrays.sort(integers, (int i1, int i2) -> i1 - i2);           // Cannot infer functional interface type
    System.out.println(Arrays.toString(integers));


    System.out.println("\nQ56 - chaining functions - compilation fails");
    Function<String, Object> f1 = s -> s.toLowerCase();
    Function<String, Object> f2 = s -> s.toUpperCase();
//    f1.compose(f2).apply("Java"); // result of f2 is input for f1 - mismatch! - compilation fails

    System.out.println("Q56 - fix");
    Function<String, String> f3 = s -> s.toLowerCase();
    Function<String, String> f4 = s -> s.toUpperCase();
    String result = f3.compose(f4).apply("Java");
    System.out.println(result); // f4 is executed first then f3
    assert result.equals("java");

    result = f3.andThen(f4).apply("Java");
    System.out.println(result);
    assert result.equals("JAVA");

    System.out.println("\nQ59_3 - chaining functions - compilation fails");
    Function<Integer, Double> ff1 = i -> (double) (i + 2); // Line 1
    Function<Double, Integer> ff2 = d -> (int) (d * 3); // Line 2
    // Function<Number, Number> ff3 = ff2.andThen(ff1); // Line 3 // Incompatible equality constraint: Number and Double
    // Function<Number, Number> ff3 = ff1.andThen(ff2); // Line 3 // Required type: Function <Number, Number> Provided: Function <Integer, Integer> Incompatible equality constraint: Number and Integer
    Function<Integer, Integer> ff3 = ff1.andThen(ff2); // Line 3
    Integer integer = ff3.apply(1);
    System.out.println(integer);
    assert Integer.valueOf(9) == integer;


    System.out.println("\nQ60_3 - Incompatible parameter types in lambda expression");
    // Predicate<Integer> p1 = (int i) -> i % 2 == 0;  // Incompatible parameter types in lambda expression: expected Integer but found int
    // Predicate p2 = (Integer i) -> i % 2 == 0;       // Incompatible parameter types in lambda expression: expected Object but found Integer
    // Predicate p3 = (Integer i) -> (int) i % 2 == 0; // Incompatible parameter types in lambda expression: expected Object but found Integer
    Predicate p4 = (var i) -> (Integer) i % 2 == 0;


    System.out.println("\nVariables scope in lambdas");
    Validator v = new Validator(false);
//    boolean validate = validate(v, v -> v.isValid()); // variable v is already in scope
    boolean validate = validate(v, va -> v.isValid()); // fix
    System.out.println("validate=" + validate);
    assert false == v.isValid();
  }

  static boolean validate(Validator v, Predicate<Validator> predicate) {
    return predicate.test(v);
  }
}

class Validator {
  boolean valid;

  public Validator(boolean valid) {
    this.valid = valid;
  }

  public boolean isValid() {
    return valid;
  }
}
