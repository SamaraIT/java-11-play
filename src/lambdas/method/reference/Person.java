package lambdas.method.reference;

import java.util.stream.Stream;

public class Person {
  String name;

  public Person(String name) {
    this.name = name;
  }

  static void print_argument_missing() {
    System.out.println("John");
  }

  static void print2(Person person) {
    System.out.println(person.name);
  }

  void print() {
    System.out.println(name);
  }
}

class Test {
  public static void main(String ...a) {
    Person person = new Person("John");
    Stream<Person> stream = Stream.of(person);
    stream.forEach(Person::print);
  }
}
