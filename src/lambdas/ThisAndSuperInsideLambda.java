package lambdas;

// Unlike code appearing in anonymous class declarations, the meaning of names and the this and super keywords appearing in a lambda body,
// along with the accessibility of referenced declarations, are the same as in the surrounding context (except that lambda parameters introduce new names).
// https://docs.oracle.com/javase/specs/jls/se11/html/jls-15.html#jls-15.27 - 15.27.2. Lambda Body

interface A {
  String s = "A";

  void m();
}

interface B extends A {
  String s = "B";
}

class C {
  String s = "C";
}

class D extends C {
  String s = "D";

  void print() {
    B b = () -> {
      System.out.println("Inside m method via lambda expression");
      System.out.print(s); // D
      System.out.print(this.s); // D
      System.out.print(super.s); // C
      System.out.print(B.s); // B
      System.out.print(A.s); // A
    };
    b.m();

    B b2 = new B() {
      @Override
      public void m() {
        System.out.println("\nInside m method via anonymous class");
        System.out.print(s); // B
        System.out.print(this.s); // B
        System.out.println("\nCannot call super.s -> compiler error");
      }
    };
    b2.m();
  }
}

public class ThisAndSuperInsideLambda {
  public static void main(String... args) {
    new D().print();
  }
}
