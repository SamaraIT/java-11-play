package assesment.test;

/**
 * String literals are used from the string pool. This means that s1 and s2 refer to the same object and are equal.
 */
public class StringEquals {

  public static void main(String[] args) {
    var s1 = "Java";
    var s2 = "Java";
    var s3 = "Ja".concat("va");
    var s4 = s3.intern();
    var sb1 = new StringBuilder();
    sb1.append("Ja").append("va");

    System.out.println(s1 == s2);
    System.out.println(s1.equals(s2));
    System.out.println(s1.toString() == s1.intern());

    System.out.println(s1 == s3);
    System.out.println(s1 == s4);
    System.out.println(sb1.toString() == s1);
    System.out.println(sb1.toString().equals(s1));


    System.out.println("** new String test");
    var s10 = new String("Java");
    var s20 = new String("Java");
    System.out.println(s10 == s20);
    System.out.println(s10.equals(s20));
    System.out.println(s10.toString() == s10.intern());
    System.out.println(s10 == s10.intern());
    System.out.println(s10 == s10.toString());
  }
}
