package streams;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

public class ParallelStreams_ForkJoinPool {

  public static void main(String[] args) {
    List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7);

    System.out.println("\n Parallel stream in ForkJoinPool");
    ForkJoinPool fjp = new ForkJoinPool(2);
    try {
      int sum = fjp.submit(
        () -> nums.stream()
          .parallel()
          .peek(i ->
            System.out.println(i + ":" + Thread.currentThread().getName()))
          .mapToInt(n -> n)
          .sum()
      ).get();
      System.out.println("Sum is " + sum);
    } catch (Exception e) {
      e.printStackTrace();
    }

    System.out.println("\n Parallel stream - parallelStream");
    int sum = nums.parallelStream()
      .peek(i ->
        System.out.println(i + ":" + Thread.currentThread().getName()))
      .mapToInt(n -> n)
      .sum();
    System.out.println("Sum is " + sum);

  }
}
