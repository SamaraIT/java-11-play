package streams;

import streams.model.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CollectingPlay {

  public static void main(String[] args) {
    List<Person> personList = getPeople();
    System.out.println("\n* Initial list");
    personList.forEach(System.out::println);

    System.out.println("\n* collect persons older than 34: filter -> collect(Collectors.toList()");
    List<Person> peopleAge34 = personList.stream()
      .filter(p -> p.getAge() > 34)
      .collect(Collectors.toList());
    System.out.println(peopleAge34);

    System.out.println("\n* collect(Collectors.toMap) ");
    try {
      Map<String, Integer> toMapExample =
        personList.stream()
          .collect(Collectors.toMap(
            p -> p.getName(),
            p -> p.getAge()));
      // If the mapped keys contain duplicates, an IllegalStateException is thrown when the collection operation is performed.
      // If the mapped keys might have duplicates, use toMap(Function, Function, BinaryOperator) instead.
    } catch (IllegalStateException e) {
      System.out.println(e.getMessage());
    }

    Map<String, Integer> toMapExample = personList.stream()
      .collect(Collectors.toMap(
        Person::getName,
        Person::getAge,
        (age, age2) -> { // BinaryOperator
          System.out.println("In mergeFunction with values, age=" + age + ", age2=" + age2);
          return age + age2; // sum
        }));
    System.out.println("collect(Collectors.toMap with mergeFunction " + toMapExample);

    System.out.println("\n *** groupingBy - returns Map");
    Map<Integer, List<Person>> personsByAge = personList.stream()
      .collect(Collectors.groupingBy(Person::getAge));
    System.out.println("personsByAge: " + personsByAge);

    Map<String, Set<Person>> personsByName = personList.stream()
      .collect(Collectors.groupingBy(Person::getName, Collectors.toSet()));
    System.out.println("personsByName: " + personsByName);

    Map<Integer, Long> numPeopleByAge = personList.stream()
      .collect(Collectors.groupingBy(
        Person::getAge, Collectors.counting())); // counting returns Long
    System.out.println("numPeopleByAge: " + numPeopleByAge);

    Map<Integer, List<String>> namesByAge = personList.stream()
      .collect(Collectors.groupingBy(
        Person::getAge,             // group by age
        Collectors.mapping(         // map from Person to
          Person::getName,          // .. name
          Collectors.toList())));   // collect names in a list
    System.out.println("namesByAge: " + namesByAge);

    Map<String, List<Integer>> ageByNames = personList.stream()
      .collect(Collectors.groupingBy( // extra play
        Person::getName,              // group by name
        Collectors.mapping(           // map from Person to
          p -> p.getAge() * 2,        // .. double age
          Collectors.toCollection(LinkedList::new)))); // collect double age in a LinkedList
    System.out.println("ageByNames: " + ageByNames);

    System.out.println("\n*** Partition by p.getAge() > 34 - specialized kind of groupingBy");
    Map<Boolean, List<Person>> peopleOlderThan34 =
      personList.stream()
        .collect(
          Collectors.partitioningBy(p -> p.getAge() > 34)
        );
    System.out.println("peopleOlderThan34: " + peopleOlderThan34);

    Map<Boolean, Set<Person>> peopleOlderThan34InSet = personList.stream()
      .collect(
        Collectors.partitioningBy(p -> p.getAge() > 34,
          Collectors.toSet())
      );
    System.out.println("peopleOlderThan34InSet: " + peopleOlderThan34InSet);

    Map<Boolean, LinkedList<String>> peopleOlderThan34ByNames = personList.stream()
      .collect(
        Collectors.partitioningBy(p -> p.getAge() > 34,
          Collectors.mapping(         // map from Person to
            Person::getName,          // .. double age
            Collectors.toCollection(LinkedList::new)))
      );
    System.out.println("peopleOlderThan34ByNamesInLinkedList: " + peopleOlderThan34ByNames);


    System.out.println("\n*** Summing and Averaging");
    Map<String, Double> avg = personList.stream()
      .collect(Collectors.groupingBy(
        Person::getName,
        Collectors.averagingInt(Person::getAge)));
    System.out.println("averaging age using Collectors.averagingInt: " + avg);

    Map<String, Integer> sumAge = personList.stream()
      .collect(Collectors.groupingBy(
        Person::getName,
        Collectors.summingInt(Person::getAge)));
    System.out.println("summing age using Collectors.summingInt: " + sumAge);

    List<String> list = Arrays.asList("A", "BB", "CCC", "DDDD", "CCCCC");
    double average1 = list.stream()
      .collect(Collectors.averagingDouble(s -> s.length()));
    System.out.println("Collectors.averagingDouble: " + average1);

    Double averagingInt = Stream.empty()
      .collect(Collectors.averagingInt(i -> 2));
    System.out.println("Collectors.averagingInt on empty Stream is: " + averagingInt); // 0.0

    OptionalDouble average = IntStream.empty().average();
    System.out.println("IntStream.empty().average() returns " + average);

    double average2 = list.stream()
      .mapToInt(a -> a.length()) // above example written using IntStream
      .average().orElse(0.0);
    System.out.println("averaging using Stream.mapToInt + average().orElse: " + average2);


    System.out.println("\n*** Counting, joining, maxBy, and minBy");
    Long counting = personList.stream()
      .filter(p -> p.getAge() > 34)
      .collect(Collectors.counting());
    //.count();
    System.out.println("Collectors.counting person's above 34: " + counting); // 3

    String olderPeople = personList.stream()
      .filter(p -> p.getAge() > 34)
      .map(Person::getName)
      .collect(Collectors.joining(", "));
    System.out.println("Collectors.joining person's names: " + olderPeople); // Jan, Leo, Eva

    Optional<Person> oldest = personList.stream()
      .collect(Collectors.maxBy((p1, p2) -> p1.getAge() - p2.getAge()));
//      .collect(Collectors.maxBy(Comparator.comparingInt(Person::getAge)));
//      .max(Comparator.comparingInt(Person::getAge));
    System.out.println("oldest: " + oldest); // Optional[Person{name='Leo', age=38}]

    Optional<Person> youngest = personList.stream()
      .collect(
        Collectors.minBy(Comparator.comparingInt(Person::getAge)));
    youngest.ifPresent(it -> System.out.println("youngest (ifPresent): " + it));
    youngest.ifPresentOrElse(
      it -> System.out.println("youngest (ifPresentOrElse): " + it),
      () -> System.out.println("No one is youngest (ifPresentOrElse)"));


    // --------------------------- FILE
    System.out.println("\n*** Collecting from a file using Files.lines");
    try (Stream<String> lines = Files.lines(Path.of("samara2.txt"))) {
      List<String> stringList = lines.collect(Collectors.toList());
      System.out.println(stringList); // [Leo Noa, Mia Noa, Eva Maja, Noa Noa]
      System.out.println("size: " + stringList.size()); // 4
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.out.println("\n*** Flat map from a file");
    try (Stream<String> lines = Files.lines(Path.of("samara2.txt"))) {
      long count = lines.map(l -> l.split(" ")) // Stream<String[]>
//        .map(array -> Arrays.stream(array))    // Stream<Stream<String>>
        .flatMap(array -> Arrays.stream(array))  // Stream<String>
        .filter(w -> w.equals("Noa"))
        .count();
      System.out.println("Number of times 'Noa' appears: " + count); // 4
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.out.println("\n** groupingBy and counting list");
    List<Integer> l1 = List.of(1);
    List<Integer> l2 = List.of(1, 2);
    List<Integer> l3 = List.of(2, 3);
    List<Integer> l4 = List.of(3, 4, 5);
    Collection<Long> values = Stream.of(l1, l2, l3, l4)
      .collect(Collectors.groupingBy(List::size, Collectors.counting()))
      .values();
    System.out.println(values); // [1, 2, 1]

  }

  private static List<Person> getPeople() {
    Person leo1 = new Person("Leo", 25);
    Person jan = new Person("Jan", 35);
    Person leo = new Person("Leo", 38);
    Person eva = new Person("Eva", 38);
    Person noa = new Person("Noa", 22);
    Person mia = new Person("Mia", 22);

    List<Person> personList = new ArrayList<>();
    personList.add(leo1);
    personList.add(jan);
    personList.add(leo);
    personList.add(noa);
    personList.add(eva);
    personList.add(mia);
    return personList;
  }
}
