package streams;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

public class Reduce_max_min {
  public static void main(String[] args) {
    Stream<Integer> stream1 = Stream.of(77, 7, 57, 24);
    Optional<Integer> minimumOpt = stream1.reduce((a, b) -> a < b ? a : b);
    System.out.println("1 " + minimumOpt);

    Stream<Integer> stream2 = Stream.of(77, 7, 57, 24);
    int minimum = stream2.reduce(0, (a, b) -> a < b ? a : b);
    System.out.println("2 " + minimum); // 0 - since identity is 0

    // fixing previous example
    Stream<Integer> stream22 = Stream.of(77, 7, 57, 24);
    int minimum22 = stream22.reduce(Integer.MAX_VALUE, (a, b) -> a < b ? a : b);
    System.out.println("3 " + minimum22);

    Stream<Integer> stream3 = Stream.of(77, 7, 57, 24);
    int maximum = stream3.reduce(0, (a, b) -> a > b ? a : b);
    System.out.println("4 " + maximum);

    Stream<Integer> stream4 = Stream.of(77, 7, 57, 24);
    Integer minumum2 = stream4.min(Comparator.comparing(a -> a)).orElse(0);
    System.out.println("5 " + minumum2);

    Stream<Integer> stream5 = Stream.of(77, 7, 57, 24);
    Integer minumum3 = stream5.min(Comparator.comparingInt(a -> a)).get();
    System.out.println("6 " + minumum3);

  }
}
