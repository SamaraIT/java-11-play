package streams.model;

public class Employee {
  private String name;

  public Employee(String n) {
    name = n;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }
}
