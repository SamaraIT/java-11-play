package streams;

import streams.model.Employee;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortingPlay {

  public static void main(String[] args) {
    System.out.println("\n** stream.sorted - sorted according to natural order");
    Stream<String> stream = Stream.of("Pero", "adam", "Anga", "pure");
    // The Stream.sorted() method without parameters requires a stream of Comparable elements, otherwise java.lang.ClassCastException
    stream.sorted().forEach(System.out::println);

    System.out.println("\n** Comparator.comparing & Comparator.thenComparing");
    Stream<Employee> emps = Stream.of(
      new Employee("Nathaniel"), new Employee("Jane"),
      new Employee("Steve"), new Employee("Nick"),
      new Employee("Jack"));
    System.out.println("sort by name length order, and in case name lengths are equal, such employees are sorted by name alphabetical order");
    Comparator<Employee> c1 = Comparator.comparing(e -> e.getName().length());
//    Comparator<Employee> c2 = (e1, e2) -> e1.getName().compareTo(e2.getName());
    Comparator<Employee> c2 = Comparator.comparing(Employee::getName);
    List<Employee> sl = emps
      .sorted(c1.thenComparing(c2))
      .collect(Collectors.toList());
    System.out.println(sl);

    System.out.println("\n** Comparator.reversed - byNameLengthDesc ");
    Comparator<Employee> byNameLengthDesc = Comparator.comparing((Employee e) -> e.getName().length()).reversed();
    Stream<Employee> emps2 = Stream.of(new Employee("Nathaniel"), new Employee("Steve"), new Employee("Nick"));
    List<Employee> s2 = emps2
      .sorted(byNameLengthDesc)
      .collect(Collectors.toList());
    System.out.print(s2);


    System.out.println("\n* sorted(new Comparator) ");
    MyClass m1 = new MyClass(1);
    MyClass m2 = new MyClass(2);
    MyClass m3 = new MyClass(3);
    Stream.of(m2, m1,m3)
      .sorted(new MyClass(0))
      .forEach(d -> System.out.println(d.number));

    Stream.of(m2, m1,m3)
      .sorted((n1,n2) -> n2.number - n1.number)
      .forEach(d -> System.out.println(d.number));

  }

}

class MyClass implements Comparator<MyClass> {
  int number;

  public MyClass(int number) {
    this.number = number;
  }

  @Override
  public int compare(MyClass o1, MyClass o2) {
    return o2.number - o1.number; // DESC
  }
}
