package streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class FlatMapExamples {
  public static void main(String... arg) {
    System.out.println("\n*** Flat map Stream<List<String>");
    List<String> names1 = Arrays.asList("Igor", "John");
    List<String> names2 = Arrays.asList("David", "Laura");
    Stream<List<String>> streamOfList = Stream.of(names1, names2);
    streamOfList
      .flatMap(names -> names.stream())   // Collection::stream
      .forEach(System.out::println);

    /* samara2.txt:
     * Leo Noa
     * Mia Noa
     * Eva Maja
     * Noa Noa
     */
    System.out.println("\n*** Flat map from a file");
    try (Stream<String> lines = Files.lines(Path.of("samara2.txt"))) {
      long count = lines
        .peek(a -> System.out.println("Reading line from file (String): " + a))
        .map(l -> l.split(" "))
        .peek(array -> System.out.println(" After String split: " + Arrays.toString(array)))
        .flatMap(Arrays::stream)
        .peek(a -> System.out.println("  After flatMap: " + a))
        .filter(w -> w.equals("Noa"))
        .count();
      System.out.println("Number of times 'Noa' appears in file: " + count);
    } catch (IOException e) {
      e.printStackTrace();
    }


    System.out.println("\n*** generate returns lists -> process in parallel -> random response");
    Stream<List<String>> streamFromList = Stream.generate(
      () -> Arrays.asList("aa", "bb", "cc", "dd"))
      .limit(2)
      .unordered();

    streamFromList.parallel()
      .flatMap(s -> s.stream())
      .map(String::toUpperCase)
      .forEach(s -> System.out.print(s + " "));

    System.out.println("\n Create stream from List<List> -> process in parallel -> random response");
    final List<List<String>> lists = Arrays.asList(
      Arrays.asList("aa", "bb", "cc", "dd"),
      Arrays.asList("aa", "bb", "cc", "dd"));
    Stream<List<String>> streamFromList2 = lists
      .stream()
      .unordered();

    streamFromList2.parallel()
      .flatMap(Collection::stream)
      .map(String::toUpperCase)
      .forEach(s -> System.out.print(s + " "));


    System.out.println("\n\nFlat map with Stream::of - tricky - does nothing");
    List<String> l1 = List.of("a", "b");
    List<Integer> l2 = List.of(1, 2);
    Stream.of(l1, l2)           // Stream<List<? extends Serializable>>
      .flatMap(Stream::of)      // Stream<List<? extends Serializable>>  - it is the same
      .forEach(System.out::print); // [a, b][1, 2]

    System.out.println("\n\nFlat map with Collection::stream - solution to the above");
    Stream.of(l1, l2)
      .flatMap(Collection::stream)
      .forEach(System.out::print); // ab12

    System.out.println("\nPrint out all numbers in the three provided lists without duplication?\n");
    List<Integer> i1 = List.of(1, 2, 3);
    List<Integer> i2 = List.of(2, 4, 6);
    List<Integer> i3 = List.of(3, 6, 9);
    Stream<List<Integer>> stream = Stream.of(i1, i2, i3);
    stream.flatMap(Collection::stream) // cannot use flatMapToInt since Collection::stream returns Stream<Integer> not IntStream
      .distinct()
      .peek(System.out::print)
      .noneMatch(l -> l < 0);  // has to match all elements so all of them are processed and printed out
//      .allMatch(l -> l > 0); // if predicate was l<0, only 1st element would be processed
  }
}
