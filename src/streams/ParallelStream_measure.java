package streams;

import java.time.Instant;
import java.util.stream.IntStream;

public class ParallelStream_measure {

  public static void main(String[] args) {
    System.out.println("\n stateful streams");
    final int SIZE = 400_000_000;
    final int LIMIT = 5;
    long sum, startTime, endTime, duration;

    IntStream streamSeq = IntStream.range(0, SIZE);
    startTime = Instant.now().toEpochMilli();
    sum = streamSeq
      .limit(LIMIT)
      .sum();
    endTime = Instant.now().toEpochMilli();
    duration = endTime - startTime;
    System.out.println("SEQUENCE - sum= " + sum + ", time= " + duration + " ms");

    IntStream streamPar = IntStream.range(0, SIZE);
    startTime = Instant.now().toEpochMilli();
    sum = streamPar
      .parallel()
      .limit(LIMIT)
      .sum();
    endTime = Instant.now().toEpochMilli();
    duration = endTime - startTime;
    System.out.println("PARALLEL - sum= " + sum + ", time= " + duration + " ms");

  }
}
