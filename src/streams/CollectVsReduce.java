package streams;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/stream/package-summary.html#Reduction
 */
public class CollectVsReduce {

  public static void main(String... args) {
    System.out.println("\n* collect");
    StringBuilder stringBuilder = Stream.of("a", "b")
      .collect(
        () -> new StringBuilder("c"), // supplier     - function that creates a new mutable result container
        StringBuilder::append,        // accumulator  - function that must fold an element into a result container
        (a, b) -> a.append(b)         // combiner     - function that accepts two partial result containers and merges them
      );
    System.out.println("collect - combiner is ignored in sequential streams: " + stringBuilder); // cab
    assert ("cab").equals(stringBuilder.toString());

    StringBuilder stringBuilder4 = Stream.of("a", "b")
      .parallel()
      .collect(
        () -> new StringBuilder(), // supplier - empty
        StringBuilder::append,     // accumulator
        (a, b) -> a.append(b)      // combiner - normal order - fold the elements from 2nd result into the 1st
      );
    System.out.println("collect in parallel with empty supplier: " + stringBuilder4); // ab
    assert "ab".equals(stringBuilder4.toString());

    StringBuilder stringBuilder3 = Stream.of("a", "b")
      .parallel()
      .collect(
        () -> new StringBuilder(),       // supplier - empty
        (StringBuilder a, String b) -> { // accumulator
          System.out.println("- in accumulator, a=" + a + ", b=" + b + ", thread=" + Thread.currentThread());
          a.append(b);
        },
        (StringBuilder a, StringBuilder b) -> {
          System.out.println("- in combiner, a=" + a + ", b=" + b + ", thread=" + Thread.currentThread());
          b.append(a);                  // combiner - reversed order
        }
      );
    System.out.println("collect in parallel with combiner in reversed order: " + stringBuilder3); // a - no matter how many items in stream
    assert ("a").equals(stringBuilder3.toString());
    /*
     * When having parallel stream with reverted combiner:
     * The combiner function passed to the collect method accepts results of the two sub-streams and merges them.
     * This function must fold the elements from the second result into the first one.
     * However, the given code does it the other way around: the first StringBuilder is appended to the second.
     * Consequently, the value of the first StringBuilder is untouched - it's still 'a';. This is the final result of the mutable reduction.
     */

    StringBuilder stringBuilder5 = Stream.of("a", "b")
      .parallel()
      .collect(
        () -> new StringBuilder("c"), // supplier - initial container
        StringBuilder::append,        // accumulator - 1st thread: c+a, 2nd thread: c+b, combiner: ca+cb
        (a, b) -> a.append(b)         // combiner - normal order - fold the elements from 2nd result into the 1st
      );
    System.out.println("collect in parallel with supplier: " + stringBuilder5);
    assert ("cacb").equals(stringBuilder5.toString());

    StringBuilder stringBuilder2 = Stream.of("a", "b")
      .parallel()
      .collect(
        () -> new StringBuilder("c"), // supplier
        StringBuilder::append,        // accumulator
        (a, b) -> b.append(a)         // combiner - reversed order
      );
    System.out.println("collect in parallel (combiner - reversed order): " + stringBuilder2); // ca
    assert ("ca").equals(stringBuilder2.toString());


    //----------------------------- REDUCE -----------
    System.out.println("\n* reduce - without identity returns Optional");
    Stream<Integer> oddStream = Stream.of(1, 3, 5);
    Optional<Integer> reduce = oddStream.filter(i -> i % 2 == 0).reduce(Integer::sum); // Optional.empty
    Optional optional = Optional.of(reduce); // Optional[Optional.empty]
    Optional empty = (Optional) optional.get();
    System.out.println("Optional[Optional.empty] .get(): " + empty);
    assert empty.equals(reduce);
    assert empty.equals(Optional.empty());

    OptionalInt optionalInt = IntStream.of(1, 3, 5).filter(i -> i % 2 == 0).reduce(Integer::sum);
    Optional optional1 = Optional.of(optionalInt);
    System.out.println("Optional[OptionalInt.empty] .get(): " + optional1.get()); // OptionalInt.empty
    assert optionalInt.equals(OptionalInt.empty());

    System.out.println("\n* reduce - with identity and accumulator, without combiner");
    List<Integer> list = List.of(1, 2);
    System.out.println("List = " + list);
    Integer sum = list.stream()
      .reduce(1,
        Integer::sum);    // accumulator - BinaryOperator
    System.out.println("reduce with accumulator: " + sum); //1+1 +2=4
    assert 4 == sum;

    System.out.println("\n* reduce - combiner is ignored in sequential stream");
    Integer sum2 = list.stream()
      .reduce(1,
        (res, i) -> res + i,              // accumulator - adds one element's value to partial result
        (part1, part2) -> part1 + part2); // combiner    - combine two partial results
    System.out.println("reduce with accumulator and combiner: " + sum2); // 1+1=2 +2=4
    assert 4 == sum2;

    System.out.println("\n* reduce in parallelStream");
    Integer sum3 = list.parallelStream()
      .reduce(
        1,
        Integer::sum,  // accumulator - BiFunction
        Integer::sum); // combiner    - BinaryOperator
    System.out.println("reduce with accumulator and combiner in parallelStream: " + sum3); // 1st thread: 1+1, 2nd thread: 1+2, combiner: 2+3=5
    assert 5 == sum3;

    // calculate real sum, when identity is 0
    System.out.println("\n* calculate sum using reduce in sequencial and parallel stream");
    list = List.of(1, 2, 3);
    System.out.println("List = " + list);
    int sumSeq = list.stream()
      .reduce(0,
        Integer::sum); // accumulator
    System.out.println("reduce - sum in seq: " + sumSeq);
    assert 6 == sumSeq;

    int sumPar = list.stream().parallel()
      .reduce(
        0,
        Integer::sum, // accumulator
        Integer::sum); // combiner
    System.out.println("reduce - sum in par: " + sumPar);
    assert 6 == sumPar;
    assert sumPar == sumSeq;


    System.out.println("\n* reduce - method signature: identity and accumulator have to return same type");
    Data data1 = new Data(1);
    Data data2 = new Data(2);
    Object result = Stream.of(data2, data1)
      //.reduce(1, (d1, d2) -> d1.number + d2.number); // bad return type in lambda - BinaryOperator
      //.reduce(1, (d1, d2) -> new Data(d1.number + d2.number)); // identity has to be Data
      // solution is to use map
      .map(d -> d.number)
      .reduce(1,
        (d1, d2) -> d1 + d2);
    System.out.println("reduce with 2 args: " + result); // 4
    assert result.equals(4);

    Integer result1 = Stream.of(data1, data2)
      .reduce(0,
        (d1, d2) -> d1 + d2.number,                 // accumulator - BiFunction
        // (Integer d1, Data d2) -> d1 + d2.number, // added variables type for clarity
        CollectVsReduce::add);                      // combiner - BinaryOperator - in seq stream it is not called
    System.out.println("reduce with 3 args: " + result1);  // 3
    assert 3 == result1;

    result1 = Stream.of(data1, data2)
      .parallel()
      .reduce(0,
        (d1, d2) -> d1 + d2.number,                 // accumulator - BiFunction
        // (Integer d1, Data d2) -> d1 + d2.number, // added variables type for clarity
        CollectVsReduce::add);                      // combiner - BinaryOperator
    System.out.println("reduce in parallel with 3 args: " + result1);  // 3
    assert 3 == result1;


    System.out.println("\n* Question - compile");
    Stream<Integer> integerStream = Stream.of(1, 2, 3, 4);
    integerStream.map(i -> {
      Function<Integer, Data> function = Data::new;
      return function.apply(i);
    })
      .forEach(Data::output);

    System.out.println("\n- Question - compile - written clearer");
    integerStream = Stream.of(1, 2, 3, 4);
    integerStream.map(i -> new Data(i))
      .forEach(Data::output);

  }

  static int add(int i1, int i2) {
    System.out.println("add: " + i1 + ", " + i2);
    return i1 + i2;
  }
}

class Data {
  int number;

  public Data(int number) {
    this.number = number;
  }

  void output() {
    System.out.print(number);
  }
}
