package streams;

import java.util.*;

public class MapFilterReducePlay {

  public static void main(String[] args) {

    List<Reading> readings = List.of(
      new Reading(1.1), new Reading(2.0),
      new Reading(3.1), new Reading(4.0)
    );
    System.out.println("\n Initial list");
    readings.forEach(System.out::println);

    System.out.println("\n mapToDouble-filter-peek-average on Streams");
    OptionalDouble average = readings.stream()
      .mapToDouble(r -> r.value) // DoubleStream
      .peek(v -> System.out.println("peek before filter: " + v))
      .filter(v -> v > 1.5 && v < 4)
      .peek(v -> System.out.println("peek after filter: " + v))
      .average();
    System.out.println("average: " + average); // OptionalDouble[2.55]
    System.out.println("average.getAsDouble(): " + average.getAsDouble());

    System.out.println("\n reduce on Streams");
    OptionalDouble sum = readings.stream()
      .mapToDouble(r -> r.value)
      .reduce((v1, v2) -> v1 + v2); // DoubleBinaryOperator.applyAsDouble
    if (sum.isPresent())
      System.out.println("sum: " + sum.getAsDouble()); // 10.2
    else
      System.out.println("empty optional");

    OptionalInt sumAsInt = readings.stream()
      .mapToInt(r -> (int) r.value) // IntStream
      .reduce((v1, v2) -> v1 + v2);// IntBinaryOperator.applyAsInt
    System.out.println("sumAsInt: " + sumAsInt);
    System.out.println("sumAsInt.getAsInt(): " + sumAsInt.getAsInt());

    System.out.println("\n reduce with identity on Streams");
    double sum2 = readings.stream()
      .mapToDouble(r -> r.value)
      .reduce(0, (v1, v2) -> v1 + v2);
    System.out.println("reduce with identity: " + sum2);
    assert sum2 == sum.getAsDouble();

    System.out.println("\n ** max & min on primitive streams");
    double max = readings.stream()
      .mapToDouble(r -> r.value)
      .reduce(0.0, (v1, v2) -> {
        if (v1 > v2)
          return v1;
        else
          return v2;
      });
    System.out.println("max (with identity) is: " + max); // 4.0

    OptionalDouble max2 = readings.stream()
      .mapToDouble(r -> r.value)
      .max();
    System.out.println("max2 (without identity) is: " + max);
    assert max == max2.getAsDouble();

    double min = readings.stream()
      .mapToDouble(r -> r.value)
      .reduce(0.0, (v1, v2) -> {
        if (v1 < v2)
          return v1;
        else
          return v2;
      });
    System.out.println("min with identity=0 is always 0, there are no negative numbers " + min);

    OptionalDouble min2 = readings.stream()
      .mapToDouble(r -> r.value)
      .min();
    System.out.println("min2 is: " + min2); // OptionalDouble[1.1]

    double min3 = readings.stream()
      .mapToDouble(r -> r.value)
      .reduce(Double.MAX_VALUE, (v1, v2) -> {
        if (v1 < v2)
          return v1;
        else
          return v2;
      });
    System.out.println("min3 where identity = Double.MAX_VALUE: " + min3);
    assert min3 == min2.getAsDouble();

    System.out.println("\n ** max on non primitive streams - using Comparator");
    Optional<Reading> maximum1 = readings.stream()
      .max(Comparator.comparingDouble(r -> r.value)); // ToDoubleFunction
    System.out.println(maximum1);

    Optional<Reading> maximum2 = readings.stream()
      .max(Comparator.comparing(r -> r.value)); // Function
    System.out.println(maximum2);
    assert maximum2.equals(maximum1);

    Optional<Reading> maximum3 = readings.stream()
      .max((r1, r2) -> {
        if (r1.value > r2.value)
          return 1;
        else return -1;
      });
    System.out.println(maximum3);
    assert maximum3.equals(maximum1);
  }

  static class Reading {
    double value;

    public Reading(double value) {
      this.value = value;
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder("Reading{");
      sb.append("value=").append(value);
      sb.append('}');
      return sb.toString();
    }
  }
}
