package streams;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class CreatingStreams {

  public static void main(String[] args) {
    Integer[] myNums = {1, 2, 3};
    Stream<Integer> myStream = Arrays.stream(myNums);
    long numElements = myStream.
      filter(i -> i > 1)
      .count();
    System.out.println("Number of elements in stream: " + numElements); // 2

    System.out.println("\n Stream.of");
    Stream.of(myNums)
      .forEach(System.out::println);

    System.out.println("\n map.keySet().stream() & map.values().stream()");
    Map<String, String> map = new HashMap<>();
    map.put("a", "aa");
    map.keySet().stream();
    map.values().stream();

    System.out.println("\n Files.lines");
    try (Stream<String> lines = Files.lines(Path.of("samara.txt"))) {
      lines.forEach(System.out::println);
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.out.println("\n** Primitive Streams:");

    System.out.println("\n IntStream");
    int[] myInts = {1, 2, 3};
    IntStream stream = Arrays.stream(myInts);
    stream.forEach(System.out::println);

    System.out.println("\n LongStream");
    LongStream longStream = LongStream.of(11, 22, 33);
    longStream.forEach(System.out::println);

    System.out.println("\n Stream<Integer> - mapToInt -> IntStream");
    Stream<Integer> integerStream = Stream.of(1, 2, 3);
    IntStream intStream = integerStream.mapToInt(i -> i);
    intStream.forEach(System.out::print);
//    IntStream intStream2 = integerStream.mapToInt(Integer::intValue); // lang.IllegalStateException: stream has already been operated upon or closed

    System.out.println("\n Stream.empty()");
    Stream.empty();
  }
}
