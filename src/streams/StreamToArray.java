package streams;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamToArray {
  public static void main(String[] args) {
    Stream<String> stream1 = Stream.of("A", "B");
    Stream<String> stream2 = Stream.of("A", "B");
    Object[] objects = stream1.toArray();
//    String[] strings = stream2.toArray(String[]::new); // IntFunction
    String[] strings = stream2.toArray((int count) -> new String[count]); // rewritten as lambda
    System.out.println(Arrays.toString(objects)); // [A, B]
    System.out.println(Arrays.toString(strings)); // [A, B]
  }
}
