package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class SampleStream {

  public static void main(String[] args) {
    // Java 8
    // filter(predicate), limit(count), skip(count)
    List<Integer> numbers = Arrays.asList(11, 32, 12, 54, 67, 81, 16, 55, 56, 9);
    System.out.println("numbers: " + numbers);
    assert numbers.size() == 10;

    System.out.println("\nfilter - print only values greater than 55");
    long count = numbers.stream()
      .filter(e -> e > 55)
      .peek(e -> System.out.print(e + ", ")) // 67, 81, 56
      .count();
    System.out.println("\nNumbers of elements: " + count);
    assert count == 3;

    // Java 9: takeWhile(predicate), dropWhile(predicate)
    Predicate<Integer> less_than_55 = e -> e < 55;
    System.out.println("\ntakeWhile - new feature in java 9");
    long countTakeWhile = numbers.stream()
      .takeWhile(less_than_55)
      .peek(e -> System.out.print(e + ", ")) // 11, 32, 12, 54
      .count();
    System.out.println("\nNumbers of elements: " + countTakeWhile);
    assert countTakeWhile == 4;

    System.out.println("\ndropWhile - new feature in java 9");
    long countDropWhile = numbers.stream()
      .dropWhile(less_than_55)
      .peek(e -> System.out.print(e + ", ")) // 67, 81, 16, 55, 56, 9
      .count();
    System.out.println("\nNumbers of elements: " + countDropWhile);
    assert countDropWhile == 6;

    assert numbers.size() == countDropWhile + countTakeWhile;

    System.out.println("\nfindFirst that is divisible by 7");
    List<Integer> ints = List.of(1, 6, 22, 21, 35, 36);
    Optional<Integer> result = ints.stream()
      .filter(i -> i % 7 == 0)
      .findFirst();
    result.ifPresentOrElse(
      System.out::println, // 21
      () -> System.out.println("No results found"));

    System.out.println("\nnoneMatch that is divisible by 7");
    boolean noneMatch = ints.stream().
      noneMatch(i -> i % 7 == 0);
    System.out.println("noneMatch: " + noneMatch);
    assert noneMatch == false;

    System.out.println("\nallMatch that is divisible by 7");
    boolean allMatch = ints.stream().
      allMatch(i -> i % 7 == 0);
    System.out.println("allMatch: " + allMatch);
    assert allMatch == false;

    System.out.println("\nanyMatch that is divisible by 7");
    boolean anyMatch = ints.stream().
      anyMatch(i -> i % 7 == 0);
    System.out.println("anyMatch: " + anyMatch);
    assert anyMatch == true;
  }

}
