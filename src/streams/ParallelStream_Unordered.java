package streams;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Stream;

public class ParallelStream_Unordered {

  public static void main(String[] args) {
    List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    Stream<Integer> stream = nums.stream();
    boolean ordered = stream.spliterator().hasCharacteristics(Spliterator.ORDERED);
    System.out.println("stream created from list is ordered: " + ordered);
    assert ordered == true;

    HashSet<Integer> ints = new HashSet<>();
    ints.add(1);    ints.add(2);    ints.add(3);
    Stream<Integer> str = ints.stream();
    Spliterator<Integer> spl = str.spliterator();
    ordered = spl.hasCharacteristics(Spliterator.ORDERED);
    System.out.println("isOrdered when created from set: " + ordered);
    assert ordered == false;

    // For parallel streams, relaxing the ordering constraint can sometimes enable more efficient execution
    int sum = nums.stream()
      .unordered()
      .parallel()
      .mapToInt(i -> i)
      .filter(i -> i % 2 == 0 ? true : false)
      .sum();
    System.out.println("sum of evens is: " + sum);
    assert 30 == sum;

    long startTime, duration;
    startTime = Instant.now().toEpochMilli();
    int mult = nums.stream()
      .unordered()
      .parallel()
      .reduce(1, (i1, i2) -> i1 * i2);
    duration = Instant.now().toEpochMilli() - startTime;
    System.out.println("mult= " + mult + ", time= " + duration + " ms");

    startTime = Instant.now().toEpochMilli();
    int mult_ordered = nums.stream()
      .parallel()
      .reduce(1, (i1, i2) -> i1 * i2);
    duration = Instant.now().toEpochMilli() - startTime;
    System.out.println("mult_ordered= " + mult_ordered + ", time= " + duration + " ms");
  }
}
