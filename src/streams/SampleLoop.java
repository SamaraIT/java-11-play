package streams;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SampleLoop {

  public static void main(String[] args) throws Exception {
    // Java 8
    for (int i = 0; i <= 5; i++) {
      System.out.println(i);
    }
    // using stream
    IntStream.rangeClosed(0, 5)
      .forEach(System.out::println);

    System.out.println("increment by 2 in for loop");
    for (int j = 0; j <= 5; j = j + 2) {
      System.out.println(j);
    }
    // nije dobro rjesenje jer ce otici overflow pa ce printati negativne brojeve
    System.out.println("Java 8 - hardcoded limit");
    Stream.iterate(0, e -> e + 2)
      .limit(3)
      .forEach(System.out::println);

    // Java 9
    System.out.println("Java 9 iterate in stream");
    IntStream.iterate(0, i -> i <= 5, i -> i + 2)
      .forEach(System.out::println);

    System.out.println("Java 9 iterate in stream using takeWhile");
    Stream.iterate(0, e -> e + 2)
      .takeWhile(e -> e <= 5) //
      .forEach(System.out::println);
  }
}
