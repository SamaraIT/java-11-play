package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Stream;

public class ParallelStreamsIntro {

  public static void main(String[] args) {
    List<Integer> nums = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    int sum = nums.stream()
      .parallel()      // make the stream parallel
      .peek(i ->
        System.out.println(i + ":" + Thread.currentThread().getName()))
      .mapToInt(n -> n)
      .sum();
    System.out.println("Sum is " + sum); // 28

    OptionalDouble average1 = nums.stream()
      .parallel()
      .mapToInt(n -> n)
      .average();
    System.out.println("average1 is " + average1); // OptionalDouble[4.0]

    System.out.println("\n** print parallel stream");
    nums.stream()
      .parallel()
      .forEach(System.out::println);

    List<Double> numbers = Arrays.asList(1.1, 2.2, 3.3);
    double sum1 = numbers.stream()
      .parallel()
      .mapToDouble(n -> n)
      .sum();
    System.out.println("sum is " + sum1); // 6.6

    OptionalDouble average = numbers.stream()
      .parallel()
      .mapToDouble(n -> n)
      .average();
    System.out.println("average is " + average); // OptionalDouble[2.1999999999999997]

    System.out.println("\n** reduce in parallel");
    String reduceInParallel = Stream.of("hi", "fi").parallel()
      .reduce("-", (s1, s2) -> s1 + s2);
    System.out.println("reduceInParallel: " + reduceInParallel);
    // result is -hifi if there is 1 thread or -hi-fi if there are 2 (each thread will append identity string)
    assert "-hi-fi".equals(reduceInParallel);

    System.out.println("Example of using String as a container in collect");
    String collectString = List.of("a", "b")
      .parallelStream()
      .collect(String::new, String::concat, String::concat);
    System.out.println(collectString);
    assert collectString.isEmpty(); // String is immutable and container is empty String

    System.out.println("Using StringBuilder to concat String stream");
    String concat = List.of("a", "b")
      .parallelStream()
      .collect(
        StringBuilder::new,
        StringBuilder::append,
        StringBuilder::append)
      .toString();
    System.out.println(concat);
    assert "ab".equals(concat);

    System.out.println("\n* collect in parallel");
    List<Integer> result = List.of(1, 2)
      .parallelStream()
      .collect(
        ArrayList::new,                             // supplier – a function that creates a new mutable result container.
        (ArrayList x, Integer y) -> x.add(y),       // accumulator - stateless function that must fold an element into a result container
        (ArrayList x, ArrayList y) -> x.addAll(y)); // combiner - function that accepts two partial result containers and merges them
    System.out.println(result); // [1, 2]
    // The accumulator function passed to the collect method is a BiConsumer with two parameters: the first is the reduction container and the second is a stream element.
    // Since the container is a list and stream elements are integers, the type of lambda parameter x is List and that of y is Integer.
    // The combiner function is another BiConsumer. This consumer combines partial results produced by parallel sub-streams.
    // These partial results are computed by the accumulator; hence they are lists. This means both parameters of the consumer are of type List

  }
}
