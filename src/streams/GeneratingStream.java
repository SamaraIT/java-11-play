package streams;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class GeneratingStream {
  public static void main(String[] args) {

    System.out.println("\n iterate");
    Stream.iterate(0, s -> s + 1)
      .limit(4)
      .forEach(System.out::println);

    System.out.println("\n generate");
    Stream.generate(new Random()::nextDouble)
      .limit(4)
      .forEach(System.out::println);

    System.out.println("\n rangeClosed");
    LongStream.rangeClosed(1, 3)
      .forEach(System.out::println); // 1 2 3

    System.out.println("\n range with skip");
    IntStream.range(1, 10)
      .filter(i -> i % 2 == 0)
      .peek(i -> System.out.println("peek: " + i)) // 2, 4, 6, 8
      .skip(3)
      .forEach(System.out::println); // 8
  }
}
