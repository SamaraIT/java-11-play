package optional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SampleOptional {

  public static void main(String[] args) throws Exception {
    // Java 8
    final Optional<Integer> optional = Optional.empty();
    // DO NOT USE GET
    try {
      System.out.println("Calling optional.get() on empty Optional");
      // If a value is present, returns the value, otherwise throws NoSuchElementException.
      optional.get(); // java.util.NoSuchElementException: No value present
    } catch (RuntimeException e) {
      System.out.println(e.getClass() + ", msg: " + e.getMessage());
    }

    Optional<Object> emptyOptional = Optional.ofNullable(null);
    assert emptyOptional.equals(optional);

    // orElse - If a value is present, returns the value, otherwise returns other.
    Integer i = optional.orElse(0);
    System.out.println("\noptional.orElse: " + i); // i = 0

    // If a value is present, performs the given action with the value, otherwise does nothing.
    optional.ifPresent(e -> System.out.println("optional.ifPresent: " + e)); // what about if value is not present

    // Java 9
    // ifPresentOrElse:
    optional.ifPresentOrElse(
      e -> System.out.println("ifPresentOrElse - action: " + e), // If a value is present, performs the given action with the value
      () -> System.out.println("ifPresentOrElse - emptyAction: -> empty optional")); // otherwise performs the given empty-based action

    // map
    // (Optional with value X).map(f) -> Optional with value Y
    // (Optional empty X).map(f) -> Optional empty Y
    // map transforms optional with value, does not touch empty
    Optional o1 = Stream.of(0).findFirst()
      .map(o -> null);
    assert o1.equals(Optional.empty());

    // orElse
    // value exists does not touch, just pass it
    // optional is empty, inject a value

    // if the value exists, move it forward
    // if the optional is empty, inject a new optional
    Optional<Integer> or = optional.or(() -> Optional.of(77));
    System.out.println("\noptional.or: " + or);

    // can be converted to stream
    System.out.println("\nEmpty optional to stream: " + optional.stream());

    List<Optional<String>> optionalList = List.of(
      Optional.of("Java"),
      Optional.empty(),
      Optional.of("is"),
      Optional.empty(),
      Optional.of("good"));
    System.out.println("\nRemove empty optionals");
    System.out.println("Optional List: " + optionalList);

//    In Java 8.0 you could remove empty optionals as follows:
    List<String> list8 = optionalList.stream()
      .flatMap(o -> o.isPresent() ? Stream.of(o.get()) : Stream.empty())
      .collect(Collectors.toList());
    System.out.println("list8: " + list8);

//    In Java 9.0 and later you can remove empty optionals with shorter code using flatMap():
    List<String> list9 = optionalList.stream()
      .flatMap(Optional::stream)
      .collect(Collectors.toList());
    System.out.println("list9: " + list9);

    System.out.println("\nEmpty stream.findAny.get");
    try {
      Object object = Stream.empty() // Returns an empty sequential Stream.
        .findAny()                   // Returns an Optional describing some element of the stream, or an empty Optional if the stream is empty.
        .get();                      // If a value is present, returns the value, otherwise throws NoSuchElementException.
    } catch (java.util.NoSuchElementException e) {
      e.printStackTrace();
    }

    System.out.println("\n The map() and flatMap() in Optional");
    Optional<String> s1 = Optional.of("Java");
    s1 = s1.map(String::toUpperCase);  // function returns String
    s1.ifPresent(System.out::println);

    Optional<String> s2 = Optional.of("Java");
    s2 = s2.flatMap(val -> Optional.of(val.toUpperCase()));// function returns Optional<String>
    s2.ifPresent(System.out::println);
  }

}
