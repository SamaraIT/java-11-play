package optional;

import java.util.Optional;
import java.util.stream.Stream;

public class OptionalWhiz {
  public static void main(String[] args) {
    System.out.println("\n* Q57");
    Integer j = Stream.of(0)
      .findAny()
      .orElse(Test.increase()); // the value passed to orElse method isn't used, but the method increase is called :)
    System.out.println(Test.i + " " + j);

    System.out.println("\n* Q63");
    Object object = Stream.empty().findAny()
      .or(() -> Optional.of(1))
      .orElseGet(() -> 2);
    System.out.println(object);

    System.out.println("\n* filter on Optional");
    Stream<Integer> stream = Stream.of(1, 3);
    Optional<Integer> optional = stream
      .findAny()
      .filter(i -> i % 2 == 0);
    try {
      System.out.println(optional.get()); //java.util.NoSuchElementException: No value present
    } catch (RuntimeException e) {
      e.printStackTrace();
    }

    System.out.println("\n* empty stream -> optional.map orElseGet");
    Integer integer = Stream.<Data>empty()
      .findAny()
      .map(d -> d.number)
      .orElseGet(() -> 1);
    System.out.println("orElseGet: " + integer); // 1
  }

}

class Test {
  static int i;

  static int increase() {
    System.out.println("increase called " + i);
    return ++i;
  }
}

class Data {
  int number;

  public Data(int number) {
    this.number = number;
  }
}
